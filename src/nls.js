export const nls = {
    headers_text: "The scale of the challenges facing our planet can seem daunting, but we can all do something.",
    headers_input_text: "Find the place to help",
    apply: "Apply",
    our_top_team: "Our Top Team",
    team_paragraph: "Learn more about how you can save our planet's nature.",
    login_section_title: "Get started today!",
    login_section_paragraph: "Learn more about how you can save our planet's nature. From smart consumption to switching to renewable energy, each of us can do our part to save the planet.",
    faq_title: "Ready to Get started?",
    faq_text: "When pattern is mentioned in interior design, it is easy to asso- ciate it with a geometric patterned wallpaper or colourful prints on interior fabrics.",
    contact_us: "CONTACT US",
    header_placeholder: "Find the place to help",
    search_button_text: "SEARCH",

}