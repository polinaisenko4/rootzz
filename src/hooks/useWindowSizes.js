import { useState, useLayoutEffect } from 'react';

export default function useWindowSizes() {
    const [size, setSize] = useState([window.innerWidth, window.innerHeight]);
    useLayoutEffect(() => {
        const listener = () => {
            setSize([window.innerWidth, window.innerHeight]);
        }
        window.addEventListener('resize', listener);
        return () => {
            window.removeEventListener('resize', listener);
        }
    }, []);
    return size;
}
