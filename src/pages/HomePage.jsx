import TeamSection from "../sections/TeamSection/TeamSection";
import FormSection from "../sections/FormSection/FormSection";
import FAQSection from "../sections/FAQSection/FAQSection";
import SliderSection from "../sections/SliderSection/SliderSection";
import FooterSection from "../sections/FoooterSection/FooterSection";
import ParrotSection from "../sections/ParrotSection/ParrotSection";

const HomePage = () => {
    return (
        <div>
            <ParrotSection />
            <TeamSection />
            <FormSection />
            <FAQSection />
            <SliderSection />
            <FooterSection />
        </div>
    )
}

export default HomePage