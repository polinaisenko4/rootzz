import scss from "./SoonPage.module.scss"
const SoonPage = () => {
    return (
        <div className={scss.soon_page}>
           SOON
            <div className={scss.center}>
                <div className={scss.wave}></div>
                <div className={scss.wave}></div>
                <div className={scss.wave}></div>
                <div className={scss.wave}></div>
                <div className={scss.wave}></div>
                <div className={scss.wave}></div>
                <div className={scss.wave}></div>
                <div className={scss.wave}></div>
                <div className={scss.wave}></div>
                <div className={scss.wave}></div>
            </div>
            <p>This page is currently being developed.</p>
        </div>
    )
}

export default SoonPage