import './App.scss';
import HeaderSection from "./sections/HeaderSection/HeaderSection";
import {Route, Routes} from "react-router-dom";
import HomePage from "./pages/HomePage";
import SoonPage from "./pages/SoonPage/SoonPage";

function App() {
    return (
        <div className="App">
            <HeaderSection />
            <Routes>
                <Route path="/" element={<HomePage />} />
                <Route path="/mission" element={<SoonPage />} />
                <Route path="/places" element={<SoonPage />} />
                <Route path="/team" element={<SoonPage />} />
            </Routes>
        </div>


    );
}
export default App;
