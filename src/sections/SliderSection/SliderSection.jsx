import scss from "./SliderSection.module.scss"
import Sliders from "../../components/Slider/Sliders";
const SliderSection = () => {
    return (
        <section className={scss.sliderSection}>
            <Sliders />
        </section>
    )
}

export default SliderSection