import scss from "./FooterSection.module.scss"

import twitter from "../../images/Twitter.svg"
import facebook from "../../images/Facebook.svg"
import linkedin from "../../images/LinkedIn.svg"

const FooterSection = () => {
    return (
        <footer className={scss.footer_wrap}>
            <div className={scss.footer}>
                <div className={scss.footer_column}>
                    <h3 className={scss.footer_column_title}>CONTACTS</h3>
                    <p className={scss.footer_column_text}>2019 Rootz Foundation.</p>
                    <p className={scss.footer_column_text}>All rights reserved</p>
                </div>
                <div className={scss.footer_address}>
                    <div className={scss.footer_column}>
                        <h4 className={scss.footer_column_title}>Headquarters</h4>
                        <p className={scss.footer_column_text}>1234 Taliban</p>
                        <p className={scss.footer_column_text}>Los Angeles, La 1234567</p>
                        <p className={scss.footer_column_text}>(123) 456-7890</p>
                    </div>
                    <div className={scss.footer_column}>
                        <h4 className={scss.footer_column_title}>Social media</h4>
                        <div className={scss.footer_socialMedia_wrap}>
                            <div className={scss.footer_socialMedia}>
                                <a href={"https://twitter.com/"} target="_blank"><img src={twitter} alt={"twitter"}/></a>
                            </div>
                            <div className={scss.footer_socialMedia}>
                                <a href={"https://www.facebook.com/"} target="_blank"><img src={facebook} alt={"facebook"}/></a>
                            </div>
                            <div className={scss.footer_socialMedia}>
                                <a href={"https://www.linkedin.com/"} target="_blank"><img src={linkedin} alt={"linkedin"}/></a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </footer>
    )
}

export default FooterSection