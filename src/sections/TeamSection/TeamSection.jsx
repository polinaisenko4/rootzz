import Paragraph from "../../components/Paragraph/Paragraph";
import {nls} from "../../nls";
import scss from "./TeamSection.module.scss"
import member1 from "../../images/team/img 1.svg"
import member2 from "../../images/team/img 2.svg"
import member3 from "../../images/team/img 3.svg"

const TeamSection = () => {
    return (
        <section className={scss.teamSection}>
            <div className={scss.teamSection_paragraph}>
                <Paragraph
                    text={nls.team_paragraph}
                    title={nls.our_top_team}
                />
            </div>
            <div className={scss.teamSection_members}>
                <div className={scss.teamSection_member}>
                    <img src={member1} alt={"member1"}/>
                </div>
                <div className={scss.teamSection_member}>
                    <img src={member2} alt={"member2"}/>
                </div>
                <div className={scss.teamSection_member}>
                    <img src={member3} alt={"member3"}/>
                </div>
            </div>
        </section>
    )
}

export default TeamSection