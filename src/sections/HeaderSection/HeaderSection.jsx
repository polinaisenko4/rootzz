import scss from "./HeaderSection.module.scss"
import Header from "../../components/Header/Header"
import useWindowSizes from "../../hooks/useWindowSizes"
import NavMenu from "../../components/NavMenu/NavMenu"
import {useState} from "react";
import close from "../../images/icon-close.svg"
import menu from "../../images/menu.svg"
import MainButton from "../../components/MainButton/MainButton"
import {nls} from "../../nls";

import { CSSTransition } from 'react-transition-group';
import "./headerAnimation.scss"

const HeaderSection = () => {
    const [burgerMenu, setBurgerMenu] = useState(false)

    const navLink = [
        {
            navigate: "/",
            title: "Home"
        },
        {
            navigate: "/mission",
            title: "Our misson"
        },
        {
            navigate: "/places",
            title: "Places"
        },
        {
            navigate: "/team",
            title: "Team"
        }

    ]

    const [width] = useWindowSizes();
    const icon = burgerMenu ? close : menu

    const openBurgerMenu = () => {
        setBurgerMenu(!burgerMenu)
    }

    const closeBurger = () => {
        setBurgerMenu(false)
    }

    return (
        <div className={scss.headerSection}>
            <Header navLink={navLink} openBurgerMenu={openBurgerMenu} icon={icon} closeBurger={closeBurger}/>
            {width < 860 &&
                    <CSSTransition
                        in={burgerMenu}
                        timeout={500}
                        classNames="element"
                        unmountOnExit>
                        <div className={scss.headerSection_burgerMenu}>
                            <NavMenu navLink={navLink} closeBurger={closeBurger}/>
                            <MainButton
                                buttonText={nls.apply}
                                textColor="#000"
                                bgColor="#fff"
                            />
                        </div>
                    </CSSTransition>
            }
        </div>
    )
}

export default HeaderSection