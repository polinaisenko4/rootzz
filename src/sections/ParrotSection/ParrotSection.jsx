import scss from "./ParrotSection.module.scss";
import headline from "../../images/headline.svg";
import {nls} from "../../nls";
import InputField from "../../components/InputField/InputField";
import MainButton from "../../components/MainButton/MainButton";
import parrot from "../../images/parrot.svg";
import card from "../../images/card.svg";

const ParrotSection = () => {
    const onClickSearch = () => {
        console.log("onClickSearch")
    }
    return (
        <section className={scss.parrotSection_wrap}>
            <div className={scss.parrotSection_nature}>
                <div className={scss.parrotSection_headline}>
                    <div className={scss.parrotSection_headline_img}>
                        <img src={headline} alt={"headline"}/>
                    </div>
                    <div className={scss.parrotSection_headline_text}>
                        <p>{nls.headers_text}</p>
                        <div className={scss.parrotSection_search}>
                            <InputField placeholder={nls.header_placeholder}/>
                            <MainButton
                                buttonText={nls.search_button_text}
                                textColor={"#fff"}
                                bgColor={"#F26C74"}
                                onClick={onClickSearch}/>
                        </div>
                    </div>
                </div>
                <div className={scss.parrotSection_parrot}>
                    <div className={scss.parrotSection_parrot_img}>
                        <img src={parrot} alt={"parrot"}/>
                    </div>
                    <div className={scss.parrotSection_parrot_members}>
                        <img src={card} alt={"card"}/>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default ParrotSection