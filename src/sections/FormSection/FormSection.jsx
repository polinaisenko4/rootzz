import scss from "./FormSection.module.scss"
import Paragraph from "../../components/Paragraph/Paragraph";
import {nls} from "../../nls";
import InputField from "../../components/InputField/InputField";
import {useState} from "react";
import MainButton from "../../components/MainButton/MainButton";


const FormSection = () => {

    const [name, setName] = useState("")
    const [email, setEmail] = useState("")

    const changeName = (e) => {
        setName(e.target.value)
    }

    const changeEmail = (e) => {
        setEmail(e.target.value)
    }

    const setForm = () => {
        const formValue = {
            name,
            email
        }
        console.log(formValue)
    }

    return (
        <section className={scss.formSection}>
            <div className={scss.formSection_paragraph}>
                <Paragraph title={nls.login_section_title} text={nls.login_section_paragraph}/>
            </div>
            <div className={scss.formSection_form}>
                <h4 className={scss.formSection_form_header}>Log in</h4>
                <InputField label={"Name"} value={name} onChange={changeName} placeholder={"Name"}/>
                <InputField label={"Email"} value={email} onChange={changeEmail} placeholder={"Email"}/>
                <MainButton onClick={setForm} textColor={"#fff"} buttonText={"Book a demo"} bgColor={"#F26C74"}/>
            </div>
        </section>
    )
}

export default FormSection