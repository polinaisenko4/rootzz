import Paragraph from "../../components/Paragraph/Paragraph";
import scss from "./FAQSection.module.scss"
import {nls} from "../../nls"
import MainButton from "../../components/MainButton/MainButton";
import Question from "../../components/Question/Question";
import {questions} from "../../components/Question/questionData";

const FAQSection = () => {

    const onClickContact = () => {
        console.log("onClickContact")
    }


    return (
        <section className={scss.faqSection}>
            <div className={scss.faqSection_paragraph}>
                <Paragraph text={nls.faq_text} title={nls.faq_title}/>
                <div className={scss.faqSection_button}>
                    <MainButton buttonText={nls.contact_us} bgColor={"#F26C74"} textColor={"#fff"} onClick={onClickContact}/>
                </div>
            </div>
            <div className={scss.faqSection_wrapper}>
                {questions.map((question) => {
                    return (
                        <Question key={question.id} title={question.title}  text={question.answer}/>
                    )
                })}
            </div>
        </section>
    )
}

export default FAQSection