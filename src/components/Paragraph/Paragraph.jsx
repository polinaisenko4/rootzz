import scss from "./Paragraph.module.scss"
const Paragraph = ({title, text}) => {
    return (
        <div className={scss.paragraph}>
            <h2 className={scss.paragraph_title}>
                {title}
            </h2>
            <p className={scss.paragraph_text}>
                {text}
            </p>
        </div>
    )
}

export default Paragraph