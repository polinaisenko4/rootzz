const NavBurgerMenu = ({openBurgerMenu, icon}) => {
    const handleClick = (e) => {
        openBurgerMenu()
    }

    return (
        <div>
            <button onClick={(e) => handleClick(e)}>
                <img src={icon} alt={"icon"}/>
            </button>

        </div>
    )
}

export default NavBurgerMenu