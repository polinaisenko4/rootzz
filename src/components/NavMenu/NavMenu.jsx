import scss from "../NavMenu/NavMenu.module.scss";
import {NavLink} from "react-router-dom";


const NavMenu = ({navLink, closeBurger}) => {

    return (
        <nav>
            <ul className={scss.nav}>
                {navLink.map((link, i) => {
                   return <NavLink
                       onClick={closeBurger}
                       key={i}
                       className={({ isActive }) => isActive ? "nav_is-active" : ""}
                       to={link.navigate}>{link.title}</NavLink>
                })}
            </ul>
        </nav>
    )
}

export default NavMenu