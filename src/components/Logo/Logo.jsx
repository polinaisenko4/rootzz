import scss from "./Logo.module.scss";
import {NavLink} from "react-router-dom";

const Logo = ({closeBurger}) => {

    return (
        <NavLink className={scss.logo} to={"/"} onClick={() => {closeBurger()}}>
            <div className={scss.logoFlower}>
                <span className={scss.petal}><img src={require("../../images/logo/Vector.svg").default} alt={"flower"}/></span>
                <span className={scss.petal}><img alt={"flower"} src={require("../../images/logo/Vector-1.svg").default}/></span>
                <span className={scss.petal}><img alt={"flower"} src={require("../../images/logo/Vector-2.svg").default}/></span>
            </div>
            <div>
                <img src={require("../../images/logo/Rootz.svg").default} alt={"rootz"}/>
            </div>
        </NavLink>
    )
}

export default Logo

