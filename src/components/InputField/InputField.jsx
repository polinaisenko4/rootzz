import scss from "./InputField.module.scss"
const InputField = ({label, value, onChange, placeholder}) => {

    const handleInput = (e) => {
        onChange(e)
    }

    return (
        <div className={scss.inputField}>
            <input
                label={label}
                placeholder={placeholder}
                value={value}
                onChange={(e) => {handleInput(e)}}
            />
        </div>
    )
}

export default InputField