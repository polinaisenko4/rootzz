import scss from "./MainButton.module.scss"

const MainButton = ({buttonText, textColor, bgColor, onClick}) => {

    const handleClick = (e) => {
        e.preventDefault()
        onClick(e)
    }

    return (
        <button
            className={scss.button}
            style={{color: textColor, backgroundColor: bgColor}}
            onClick={(e) => handleClick(e)}>
            {buttonText}
        </button>
    )
}

export default MainButton