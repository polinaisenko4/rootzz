import React from "react";
import "./Sliders.scss"
import { Swiper, SwiperSlide, } from "swiper/react";
import { Pagination, Navigation } from "swiper";

import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";

import {sliderData} from  "./SliderData"
import bg from "../../images/bg.svg"



export default function Sliders() {
    return (
        <>
            <Swiper
                loop={true}
                speed={1500}
                pagination={{
                    type: "fraction",
                }}
                navigation={true}
                modules={[Pagination, Navigation]}
                centeredSlides={true}
                breakpoints={{
                    1074: {
                        slidesPerView: 3,
                    },
                }}
            >
                {
                    sliderData.map((card) => {
                        return (
                            <SwiperSlide key={card.id}>
                                {({ isActive, isNext }) => (
                                    isActive
                                        ?
                                        <div className="slide_active_card">
                                            <img src={bg} alt={'bg'}/>
                                            <div>
                                                <h4>{card.title}</h4>
                                                <p>{card.text}</p>
                                            </div>
                                            <div className="slide_active_card_img">
                                                <img src={require( `../../images/slider/${card.img}`)} alt={"props.imageAlt"} />
                                            </div>
                                        </div>
                                        :
                                        <div className="slide_card_img">
                                            <h4>{card.title}</h4>
                                            <img src={require( `../../images/slider/${card.img}`)} alt={"props.imageAlt"} />
                                        </div>
                                )}
                            </SwiperSlide>
                        )
                    })
                }

            </Swiper>
        </>
    );
}
