export const sliderData = [
    {
        id: 0,
        title: "Save watter",
        text: "Taking on the issue of ensuring access to safe water requires worldwide effort.",
        img: "watter.png"
    },
    {
        id: 1,
        title: "Choose organic",
        text: "Taking on the issue of ensuring access to safe water requires worldwide effort.",
        img: "organic.png"
    },
    {
        id: 2,
        title: "Plant trees",
        text: "Taking on the issue of ensuring access to safe water requires worldwide effort.",
        img: "plant.png"
    },
    {
        id: 3,
        title: "Avoid plastic",
        text: "Taking on the issue of ensuring access to safe water requires worldwide effort.",
        img: "plastic.png"
    },
    {
        id: 4,
        title: "Save energy",
        text: "Taking on the issue of ensuring access to safe water requires worldwide effort.",
        img: "energy.png"
    },
];