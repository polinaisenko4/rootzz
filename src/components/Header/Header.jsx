import scss from "./Header.module.scss"
import {nls} from "../../nls";
import useWindowSizes from "../../hooks/useWindowSizes";
import Logo from "../Logo/Logo";
import NavMenu from "../NavMenu/NavMenu";
import MainButton from "../MainButton/MainButton";
import NavBurgerMenu from "../NavBurgerMenu/NavBurgerMenu";
import {useState} from "react";

const Header = ({openBurgerMenu, icon, navLink, closeBurger}) => {
    const [width] = useWindowSizes();
    const [apply, setApply] = useState(false)

    const onClickApply = () => {
        if(!apply) {
            setApply(true)
        }
    }

    return (
        <header className={scss.header}>
            <Logo closeBurger={() => {closeBurger()}}/>
            {width > 860 ?
                <>
                    <NavMenu navLink={navLink}/>
                    <div className={scss.header_button}>
                        <MainButton
                            buttonText={nls.apply}
                            textColor="#000"
                            bgColor="#fff"
                            onClick={onClickApply}
                        />
                    </div>
                </>
                :
                <>
                    <NavBurgerMenu openBurgerMenu={openBurgerMenu} icon={icon}/>
                </>

            }
        </header>
    )
}

export default Header