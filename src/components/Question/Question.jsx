import scss from "./Question.module.scss"
import {useState} from "react"
import close from "../../images/close.svg"
import open from "../../images/open.svg"

const Question = ({title, text}) => {
    const [answer, setAnswer] = useState(false)
    const icon = answer ? close : open;
    const openAnswer = () => {
        setAnswer(!answer)
    }


    return (
        <div className={scss.question}>
            <div className={scss.question_title} onClick={(e) => {openAnswer()}}>
                <h4>{title}</h4>
                <button className={scss.question_button}>
                    <img src={icon} alt={title}/>
                </button>
            </div>
            <div className={`${scss.question_answer} ${answer ? scss.question_answer_active : ""}`}>
                <p >
                    {answer ? text : ""}
                </p>
            </div>
        </div>
    )
}

export default Question